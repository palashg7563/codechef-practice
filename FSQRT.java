import java.util.Scanner;

class FSQRT{
  public static void main(String[] args) {
    Scanner kb=new Scanner(System.in);
    int test = kb.nextInt();
    int a[] = new int[test];
    int ans[] = new int[test];
    for(int i=0;i<test;i++){
      a[i]=kb.nextInt();
    }
    for(int j=0;j<test;j++){
      ans[j] = (int)(Math.sqrt(a[j]));
    }
    for(int k=0;k<test;k++){
      System.out.println(ans[k]);
    }
  }
}