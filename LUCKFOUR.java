import java.util.Scanner;

class LUCKFOUR{
  public static void main(String[] args) {
    Scanner kb=new Scanner(System.in);
    int test = kb.nextInt();
    int a[] = new int[test];
    int ans[] = new int[test];
    for(int i=0;i<test;i++){
      a[i]=kb.nextInt();
    }
    for(int j=0;j<test;j++){
      int count =0;
      int no = a[j];
      for(int k=0;no!=0;k++){
        if(no%10==4){
          count++;
        }
        no=no/10;
      }
      ans[j]=count;
    }
    for(int p=0;p<ans.length;p++){
      System.out.println(ans[p]);
    }
  }
}