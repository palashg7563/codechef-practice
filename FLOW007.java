import java.util.Scanner;

class FLOW007{
  public static void main(String[] args) {
    Scanner kb=new Scanner(System.in);
    int test=kb.nextInt();
    int[] a=new int[test];
    int[] ans = new int[test];
    for(int i=0;i<test;i++){
      a[i]=kb.nextInt();
    }
    for(int j=0;j<test;j++){
      StringBuffer s = new StringBuffer();
      String s1 = Integer.toString(a[j]);
      s.append(s1);
      s.reverse();
      String ans1 = s.toString();
      int ans2 = Integer.parseInt(ans1);
      ans[j] = ans2;
    }
    for(int k=0;k<ans.length;k++){
      System.out.println(ans[k]);
    }
  }
}